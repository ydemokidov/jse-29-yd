package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlXmlLoadCommand extends AbstractFasterXmlLoadCommand {

    @NotNull
    private final String name = "load_fasterxml_xml";

    @NotNull
    private final String description = "Load XML with FasterXml library";

    @Override
    @NotNull
    protected ObjectMapper getMapper() {
        return new XmlMapper();
    }

    @Override
    @NotNull
    protected String getFile() {
        return FILE_XML;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
