package com.t1.yd.tm.command.system;

import com.t1.yd.tm.util.FormatUtil;
import org.jetbrains.annotations.NotNull;

public class SystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "Show info about system";

    @Override
    public void execute() {
        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory : " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory : " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory : " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        System.out.println("Used memory : " + usedMemoryFormat);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
