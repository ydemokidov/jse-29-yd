package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.Domain;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJaxbJsonSaveCommand extends AbstractDataCommand {

    private final String name = "save_jaxb_json";

    private final String description = "Save JSON with JAXB library";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON JAXB]");

        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        marshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
