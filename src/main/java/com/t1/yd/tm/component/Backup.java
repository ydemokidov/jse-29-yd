package com.t1.yd.tm.component;

import com.t1.yd.tm.command.data.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.processCommand("save_backup", false);
    }

    public void load() {
        if (Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) bootstrap.processCommand("load_backup", false);
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            Thread.sleep(3000);
            save();
        }
    }
}
